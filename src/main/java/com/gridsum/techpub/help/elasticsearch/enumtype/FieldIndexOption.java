package com.gridsum.techpub.help.elasticsearch.enumtype;

public enum FieldIndexOption {
    Analyzed,
    NotAnalyzed,
    No,
}
