package com.gridsum.techpub.help.elasticsearch.annotation;

import com.gridsum.techpub.help.elasticsearch.enumtype.FieldIndexOption;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldType;
import com.gridsum.techpub.help.elasticsearch.enumtype.SubType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME) // 注解会在class字节码文件中存在，在运行时可以通过反射获取到
@Target({ElementType.FIELD})
public @interface ElasticProperty {
    FieldType type();
    String analyzer() default "ik_max_word";
    String nullValue() default "-";
    FieldIndexOption index() default FieldIndexOption.No;
    boolean includeInAll() default false;
    String format() default "";
    int ignore_above() default 0;
    SubType subtype() default SubType.None;
}
