package com.gridsum.techpub.help.elasticsearch;


import com.gridsum.techpub.help.elasticsearch.enumtype.SubType;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Mapping {
    private static Map<String,String> mapping = new HashMap<>();
    private static Map<SubType,String> subtypeMapping = new HashMap<>();
    private static Map<SubType,String> subtypeMapping6 = new HashMap<>();
    static {
        //byte Byte
        mapping.put(Byte.class.getName(),"short");
        mapping.put("byte","short");

        //short Byte
        mapping.put(Short.class.getName(),"short");
        mapping.put("short","short");

        //int Integer
        mapping.put(Integer.class.getName(),"integer");
        mapping.put("int","integer");

        //int Integer
        mapping.put(Long.class.getName(),"long");
        mapping.put("long","long");

        //Float float
        mapping.put(Float.class.getName(),"float");
        mapping.put("float","float");

        //Double double
        mapping.put(Double.class.getName(),"double");
        mapping.put("double","double");

        //Boolean
        mapping.put(Boolean.class.getName(),"boolean");
        mapping.put("boolean","boolean");

        //String
        mapping.put(String.class.getName(),"string");

        //Date
        mapping.put(Date.class.getName(),"date");


        //subtypeMapping

        subtypeMapping.put(SubType.None,"");
        subtypeMapping.put(SubType.Keyword,"\"fields\": {\n" +
                "            \"keyword\": { \n" +
                "              \"type\":  \"string\",\n" +
                "              \"index\": \"not_analyzed\"\n" +
                "            }\n" +
                "          },");
        subtypeMapping.put(SubType.Text,"\"fields\": {\n" +
                "            \"text\": { \n" +
                "              \"type\":  \"string\",\n" +
                "              \"analyzer\": \"ik_max_word\"\n" +
                "            }\n" +
                "          },");
        subtypeMapping6.put(SubType.Keyword,"\"fields\": {\n" +
                "            \"keyword\": { \n" +
                "              \"type\":  \"keyword\"\n" +
                "            }\n" +
                "          },");
        subtypeMapping6.put(SubType.Text,"\"fields\": {\n" +
                "            \"text\": { \n" +
                "              \"type\":  \"text\",\n" +
                "              \"analyzer\": \"ik_max_word\"\n" +
                "            }\n" +
                "          },");
        subtypeMapping6.put(SubType.PinYin,"\"fields\": {\n" +
                "            \"pinyin\": { \n" +
                "              \"type\":  \"text\",\n" +
                "              \"analyzer\": \"pinyin\"\n" +
                "            }\n" +
                "          },");


    }

    public static String getName(String name){
        return mapping.getOrDefault(name,null);
    }

    public static String getSubType(SubType subType){
        String className = new Exception().getStackTrace()[1].getClassName();
        if (className.equals(MappingBuilder.class.getName()))
            return subtypeMapping.get(subType);
        return subtypeMapping6.get(subType);
    }

}
