package com.gridsum.techpub.help.elasticsearch.exception;

public class AnnotationMissException extends RuntimeException {
    public AnnotationMissException(String message) {
        super(message);
    }
}
