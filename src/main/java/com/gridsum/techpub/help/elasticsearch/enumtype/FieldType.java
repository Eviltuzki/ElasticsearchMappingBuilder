package com.gridsum.techpub.help.elasticsearch.enumtype;

public enum FieldType {
    Nested,
    Boolean,
    Date,
    Double,
    Float,
    Byte,
    Short,
    Long,
    Integer,
    String
}
