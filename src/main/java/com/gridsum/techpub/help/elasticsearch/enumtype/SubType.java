package com.gridsum.techpub.help.elasticsearch.enumtype;

public enum  SubType {
    None,
    Text,
    Keyword,
    PinYin
}
