package com.gridsum.techpub.help.elasticsearch.model;

import com.gridsum.techpub.help.elasticsearch.Name;
import com.gridsum.techpub.help.elasticsearch.annotation.ElasticProperty;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldIndexOption;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldType;
import com.gridsum.techpub.help.elasticsearch.enumtype.SubType;

import java.util.Date;
import java.util.List;
@Name("退款申请单")
public class VenusSaleReturnBill {
    private Long id;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String isDeleted;
    private Integer creator;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date gmtCreate;
    private Long sellerId;
    private Long shopId;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = "jd_car_edge_ngram_analyzer")
    private String billNo;
    private Integer billStatus;
    private Long orderId;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = "jd_car_edge_ngram_analyzer")
    private String orderSn;
    private Double returnAmount;
    private Integer warehouseId;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String warehouseName;
    private Integer transportStyle;
    private Double shippingFee;
    private Integer shippingFeeBearer;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,subtype = SubType.PinYin)
    private String customerName;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String returnReason;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = "jd_car_edge_ngram_analyzer")
    private String customerPhone;
    private Integer auditorId;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String auditorName;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date gmtAudit;
    private Integer unAuditorId;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String unAuditorName;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String unAuditReason;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String gmt_un_audit;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String returnComment;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String cardUrl;
    private Long orderStatus;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String tradeStatus;
    private Double orderAmount;
    private Long province;
    private Long city;
    private Long district;
    private Long street;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String address;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String sellerContactsMobile;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,subtype = SubType.PinYin)
    private String sellerName;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String)
    private String sellerCode;
    @ElasticProperty(type = FieldType.Nested)
    private List<GoodsList> goodsList;
}
class GoodsList {
    private Long id;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String isDeleted;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,subtype = SubType.PinYin)
    private String goodsName;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = "jd_car_edge_ngram_analyzer")
    private String goodsFormat;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = "jd_car_edge_ngram_analyzer")
    private String goodsSn;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String goodsId;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,subtype = SubType.PinYin)
    private String brandName;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String minMeasureUnit;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String measureUnit;
    private Integer conversionValue;
    private Double returnPrice;
    private Integer returnQty;
    private Double returnAmount;
    private Integer billId;
    private Integer orderGoodsId;
}