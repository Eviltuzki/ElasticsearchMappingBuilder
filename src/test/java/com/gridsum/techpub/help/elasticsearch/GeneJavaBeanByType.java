package com.gridsum.techpub.help.elasticsearch;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class GeneJavaBeanByType {
    public static void main(String[] args) throws IOException {
        List<String> allLines = Files.readAllLines(Paths.get("D:\\Project\\java\\ElasticsearchMappingBuilder\\src\\test\\java\\com\\gridsum\\techpub\\help\\elasticsearch\\File.txt"));
        StringBuilder code = new StringBuilder();
        for (String line : allLines) {
            List<String> cells = Arrays.stream(line.split("\t")).collect(Collectors.toList());
            if (cells.get(1).equals("nested")){
                code.append("@ElasticProperty(type = FieldType.Nested)").append("\n\r");
            }
            if (cells.get(1).equals("date")){
                code.append("@ElasticProperty(type = FieldType.Date,format = \"yyyy-MM-dd hh:mm:ss\")").append("\n\r");
            }
            if (cells.get(3).toLowerCase().equals("false")&&(cells.get(1).equals("keyword")||cells.get(1).equals("text")||cells.get(1).equals("string")))
                code.append("@ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)").append("\n\r");
            if (cells.get(3).toLowerCase().equals("true")&&(cells.get(1).equals("keyword")||cells.get(1).equals("string")))
                code.append("@ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)").append("\n\r");
            if (cells.size() == 6){
                if (cells.get(5).equals("ik_max_word"))
                    code.append("@ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String)").append("\n\r");
                else
                    code.append("@ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = \"jd_car_edge_ngram_analyzer\")").append("\n\r");
            }
            code.append("private ").append(getValueType(cells.get(1).toLowerCase().trim())).append(" "+cells.get(0) + ";").append("\n\r");
        }


        System.out.println(code.toString());
    }

    private static String getValueType(String original) {
        switch (original){
            case "long":
                return "Long";
            case "double":
                return "Double";
            case "integer":
                return "Integer";
            case "keyword":
                return "String";
            case "text":
                return "String";
            case "string":
                return "String";
            case "date":
                return "Date";
            case "float":
                return "Float";
            case "byte":
                return "Byte";
            case "nested":
                return "Object";
            case "boolean":
                return "Boolean";
                default:
                    throw new IllegalArgumentException(original);
        }
    }
}
