package com.gridsum.techpub.help.elasticsearch.model;

import com.gridsum.techpub.help.elasticsearch.Name;
import com.gridsum.techpub.help.elasticsearch.annotation.ElasticProperty;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldIndexOption;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldType;
import com.gridsum.techpub.help.elasticsearch.enumtype.SubType;

import java.util.Date;
import java.util.List;
@Name("订单信息")
public class VenusOrderInfo {
    private Long id;
    private Long shippingId;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date signDate;
    private Long saleId;
    private Long orderServiceId;
    private Double discount;
    private Integer orderStatus;
    private Integer userType;
    private Integer province;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date gmtModified;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String shippingName;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String sellerId;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,subtype = SubType.PinYin)
    private String consignee;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String shippingStatus;
    private Double shippingFee;
    private Long shopId;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,subtype = SubType.PinYin)
    private String companyName;
    private Integer planStatus;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String city;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String)
    private String postscript;
    private Long orderSourceType;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String address;
    private Integer stockoutType;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String payName;
    private Long transferStatus;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String attributes;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String saleName;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,subtype = SubType.PinYin)
    private String sellerNick;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = "jd_car_edge_ngram_analyzer")
    private String orderSn;
    private Long warehouseId;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String country;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String referer;
    private Double bonus;
    private Double orderAmount;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String businessNote;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String orderFlags;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date appointmentTime;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date payTime;
    private Long parentId;
    private Long district;
    private Long cityId;
    private Long payId;
    private Long payStatus;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String isDelete;
    private Long street;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String tradeStatus;
    private Long invStatus;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String invType;
    private Long invTypeValue;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date shippingTime;
    private Double goodsAmount;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = "jd_car_edge_ngram_analyzer")
    private String mobile;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date confirmTime;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date addTime;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String logisticsNo;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String deliverMark;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String expressCompanyName;
    private Long orderExecId;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date delayTime;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String sale1Role;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String secretaryPostscript;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String roleSignee;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String achievementDesc;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String achievementSale1;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String achievementSale2;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String sale1Area;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String userOperatorGroup;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String sale2Group;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String userOperatorDept;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String userOperatorId;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String orderExecArea;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String achievementSale1Role;
    private Long sale1Id;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String sale2Role;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String orderExec;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String sale1BigArea;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String sale2Area;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String achievementSale1BigArea;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String groupSignee;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String sale2Dept;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String sale1;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String sale2;
    private Long sale2_id;
    private Integer isInvoice;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String areaSignee;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String sale1Dept;
    private Long achievementSale2Id;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String userOperatorArea;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String userOperatorRole;
    private Long customerId;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String achievementSale2Area;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String orderExecDept;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String userSignee;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String userOperatorBigArea;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String achievementSale2Role;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String achievementSale1Dept;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String sale2BigArea;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String sale1Group;
    private Long userSigneeId;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String achievementSale1Group;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String achievementSale2Dept;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String orderExecBigArea;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String orderExecRole;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String userOperator;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String deptSgnee;
    private Long achievementSale1Id;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String bigAreaSignee;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String achievementSale1Area;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String achievementSale2BigArea;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String orderExecGroup;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String sellerCode;
    private Long provinceId;
    private Long districtId;
    private Long userId;
    private Long ucShopId;
    private Long orderId;
    private Long orderType;
    private Integer payType;
    private Long validOrderCount;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String comment;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date orderCreateTime;
    @ElasticProperty(type = FieldType.Nested)
    private List<OrderGood> orderGoods;
}
class OrderGood{
    private Long activityGroupId;
    private Long activityId;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,subtype = SubType.PinYin)
    private String activityName;
    private Long goodsId;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,subtype = SubType.PinYin)
    private String goodsName;
    private Long goodsNumber;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = "jd_car_edge_ngram_analyzer")
    private String goodsFormat;
    private Double goodsPrice;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = "jd_car_edge_ngram_analyzer")
    private String goodsSn;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String measureUnit;
    private Double soldPrice;
    private Double soldPriceAmount;
    private Double taxRate;
    private Double taxFee;
    private Double priceTaxAmount;
    private Long orderId;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = "jd_car_edge_ngram_analyzer")
    private String oeNum;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String goodsImg;
    private Integer packingValue;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String goodsQualityType;
    private Long brandId;
    private Long catId;
}