package com.gridsum.techpub.help.elasticsearch.model;

import com.gridsum.techpub.help.elasticsearch.Name;
import com.gridsum.techpub.help.elasticsearch.annotation.ElasticProperty;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldIndexOption;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldType;
import com.gridsum.techpub.help.elasticsearch.enumtype.SubType;

import java.util.Date;
@Name("云修物料")
public class LegendGoods {
    private Long id;
    private Integer brandId;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String carInfo;
    private Integer catId;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String depot;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String format;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,subtype = SubType.PinYin)
    private String goodsBrand;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,subtype = SubType.PinYin)
    private String goodsCat;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = "jd_car_edge_ngram_analyzer")
    private String goodsSn;
    private Integer goodsStatus;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String imgUrl;
    private Double inventoryPrice;
    private Double lastInPrice;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date lastInTime;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String measureUnit;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,subtype = SubType.PinYin)
    private String name;
    private Integer onsaleStatus;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,subtype = SubType.PinYin)
    private String origin;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String)
    private String partUsedTo;
    private Double price;
    private Integer shopId;
    private Long shortageNumber;
    private Double stock;
    private Integer tqmallGoodsId;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = "jd_car_edge_ngram_analyzer")
    private String tqmallGoodsSn;
    private Integer tqmallStatus;
    private Integer goodsTag;
    private Integer stdCatId;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,subtype = SubType.PinYin )
    private String catName;
}
