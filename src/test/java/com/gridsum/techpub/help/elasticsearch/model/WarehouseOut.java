package com.gridsum.techpub.help.elasticsearch.model;

import com.gridsum.techpub.help.elasticsearch.Name;
import com.gridsum.techpub.help.elasticsearch.annotation.ElasticProperty;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldIndexOption;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldType;

import java.util.Date;
import java.util.List;
@Name("出库列表")
public class WarehouseOut {
    private Long id;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date gmtCreate;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String customerMobile;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String carLicense;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = "jd_car_edge_ngram_analyzer")
    private String warehouseOutSn;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String carType;
    private Long goodsReceiver;
    private Integer shopId;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String out_type;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String comment;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String customerName;
    private Long orderId;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String status;
    @ElasticProperty(type = FieldType.Nested)
    private List<Detail> detail;

}
class Detail{
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String goodsName;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String goodsSn;
    private Long warehouseOutId;
    private Long id;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String carInfo;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = "jd_car_edge_ngram_analyzer")
    private String goodsFormat;
    private Double salePrice;
    private Double inventoryPrice;
    private Double goodsCount;
}