package com.gridsum.techpub.help.elasticsearch.model;

import com.gridsum.techpub.help.elasticsearch.Name;
import com.gridsum.techpub.help.elasticsearch.annotation.ElasticProperty;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldIndexOption;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldType;
import com.gridsum.techpub.help.elasticsearch.enumtype.SubType;

import java.util.Date;
import java.util.List;
@Name("供应商查询")
public class ShopAutoDealer {
    private Long id;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date gmtCreate;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,subtype = SubType.PinYin)
    private String supplierName;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String provinceName;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String cityName;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String districtName;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String supplierAddress;
    private Double score;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String telephone;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String mainBusiness;
    private Long provinceId;
    private Long cityId;
    private Long districtId;
    private Long streeId;
    @ElasticProperty(type = FieldType.Nested)
    private List<Brand> brand;
    @ElasticProperty(type = FieldType.Nested)
    private List<Follower> follower;
    @ElasticProperty(type = FieldType.Nested)
    private List<Category> category;

}

class Brand {
    private Long brandId;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,subtype = SubType.PinYin)
    private String brandName;
}

class Follower {
    private Long shopId;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date followTime;
}

class Category {
    private Long categoryOneLevelId;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String categoryOneLevelName;
    private Long categoryTwoLevelId;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String categoryTwoLevelName;

}
