package com.gridsum.techpub.help.elasticsearch.model;

import com.gridsum.techpub.help.elasticsearch.Name;
import com.gridsum.techpub.help.elasticsearch.annotation.ElasticProperty;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldIndexOption;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldType;
import com.gridsum.techpub.help.elasticsearch.enumtype.SubType;

import java.util.Date;
@Name("入库明细")
public class WarehouseIn {
    private Long id;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date gmtCreate;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,subtype = SubType.PinYin)
    private String goodsName;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = "jd_car_edge_ngram_analyzer")
    private String goodsFormat;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = "jd_car_edge_ngram_analyzer")
    private String goodsSn;
    private Double goodsCount;
    private Double purchasePrice;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String status;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = "jd_car_edge_ngram_analyzer")
    private String warehouseInSn;
    private Long warehouseInId;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String carInfo;
    private Integer shopId;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String supplierName;
    private Long supplierId;
    private Long purchaseAgent;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date inTime;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String purchaseAgentName;
    private Integer creator;
    private Double freight;
    private Double tax;
    private Double totalAmount;
}
