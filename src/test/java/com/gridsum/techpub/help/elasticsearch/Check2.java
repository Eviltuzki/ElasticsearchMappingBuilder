package com.gridsum.techpub.help.elasticsearch;

import com.gridsum.techpub.help.elasticsearch.annotation.ElasticProperty;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.Charset;

public class Check2 {
    public static void main(String[] args) throws ClassNotFoundException, IOException {
        String packageName ="com.gridsum.techpub.help.elasticsearch.model";
        String path = "D:\\Project\\java\\ElasticsearchMappingBuilder\\src\\test\\java\\com\\gridsum\\techpub\\help\\elasticsearch\\model";
        String[] list = new File(path).list((dir, name) -> {
            return name.endsWith("java");
        });
        for (String className : list) {
            String classFullName = packageName + "."+className.substring(0, className.length() - 5);
            Class<?> clz = Class.forName(classFullName);

//            for (Field field : clz.getDeclaredFields()) {
//                if (field.getType().equals(String.class)){
//                    ElasticProperty property = field.getDeclaredAnnotation(ElasticProperty.class);
//                    //System.out.println(classFullName+":"+field.getName());
//                    System.out.println(property.analyzer());
//                }
//            }
            String builder = MappingBuilder6.builder(clz);
            builder="{\"mappings\": {\"docs\":"+builder+"}}";
            FileUtils.write(new File(clz.getAnnotation(Name.class).value()+".json"),builder.toString(),Charset.defaultCharset());
        }
    }
}
