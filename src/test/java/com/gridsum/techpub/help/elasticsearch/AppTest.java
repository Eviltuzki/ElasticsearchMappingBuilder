package com.gridsum.techpub.help.elasticsearch;

import com.gridsum.techpub.help.elasticsearch.model.CloudepcCenterGoods;
import com.gridsum.techpub.help.elasticsearch.model.LegendGoods;
import com.gridsum.techpub.help.elasticsearch.model.ShopAutoDealer;
import com.gridsum.techpub.help.elasticsearch.model.WarehouseIn;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


/**
 * Unit test for simple MappingBuilder.
 */
public class AppTest extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp() throws ClassNotFoundException {
        MappingBuilder6.setDefaultAnalysis(false);

        String shopAutoDealer = MappingBuilder6.builder(ShopAutoDealer.class);
        System.out.println("shopAutoDealer");
        System.out.println(shopAutoDealer);

        String cloudepcCenterGoods = MappingBuilder6.builder(CloudepcCenterGoods.class);
        System.out.println("cloudepcCenterGoods");
        System.out.println(cloudepcCenterGoods);

        String legendGoods = MappingBuilder6.builder(LegendGoods.class);
        System.out.println("legendGoods");
        System.out.println(legendGoods);

        String warehouseIn = MappingBuilder6.builder(WarehouseIn.class);
        System.out.println("warehouseIn");
        System.out.println(warehouseIn);


    }
}
