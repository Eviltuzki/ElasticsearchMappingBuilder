package com.gridsum.techpub.help.elasticsearch;

import com.gridsum.techpub.help.elasticsearch.annotation.ElasticProperty;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldIndexOption;

import java.io.File;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.Date;

public class Check {
    public static void main(String[] args) throws ClassNotFoundException {
        String packageName ="com.gridsum.techpub.help.elasticsearch.model";
        String path = Check.class.getClassLoader().getResource("").getPath();
        path+=(packageName).replace(".","/");
        String[] list = new File(path).list((dir, name) -> {
            return name.endsWith("class");
        });
        for (String className : list) {
            String classFullName = packageName + "."+className.substring(0, className.length() - 6);
            Class<?> clz = Class.forName(classFullName);
            for (Field field : clz.getDeclaredFields()) {
                if (field.getType().equals(String.class)){
                    ElasticProperty property = field.getDeclaredAnnotation(ElasticProperty.class);
                    if (property.index().equals(FieldIndexOption.Analyzed)&&property.analyzer().contains("ik")){
                        if (property.subtype().toString().equals("None"))
                            System.out.println(classFullName+"-->"+ field.getName());
                    }

                }
            }

        }
    }
}
