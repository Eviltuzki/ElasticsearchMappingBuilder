package com.gridsum.techpub.help.elasticsearch.model;

import com.gridsum.techpub.help.elasticsearch.Name;
import com.gridsum.techpub.help.elasticsearch.annotation.ElasticProperty;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldIndexOption;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldType;
import com.gridsum.techpub.help.elasticsearch.enumtype.SubType;

import java.util.List;
@Name("电商商品主搜")
public class ShopGoods {
    private Long id;
    @ElasticProperty(index = FieldIndexOption.Analyzed, type = FieldType.String,subtype = SubType.PinYin)
    private String goodsName;
    private Long catId;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed, type = FieldType.String)
    private String catName;
    @ElasticProperty(type = FieldType.Nested)
    private List<ShopAdapterCar> adapterCar;
    @ElasticProperty(type = FieldType.Nested)
    private List<Attribute> attribute;
    private Long brandId;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed, type = FieldType.String)
    private String brandName;
    @ElasticProperty(index = FieldIndexOption.No, type = FieldType.String)
    private String brandPartcode;
    private Long carPartsType;
    private Long conversionValue;
    @ElasticProperty(index = FieldIndexOption.No, type = FieldType.String)
    private String factoryCode;
    private Long goodsCarType;
    @ElasticProperty(index = FieldIndexOption.Analyzed, type = FieldType.String, analyzer = "jd_car_edge_ngram_analyzer")
    private String goodsFormat;
    @ElasticProperty(index = FieldIndexOption.No, type = FieldType.String)
    private String goodsImg;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed, type = FieldType.String)
    private String goodsQualityType;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed, type = FieldType.String)
    private String goodsSn;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed, type = FieldType.String)
    private String goodsTag;
    private Long isQualityPeriodManage;
    private Long qualityPeriod;
    private Long warningDays;
    private Long saleableDays;
    private Long isReal;
    @ElasticProperty(index = FieldIndexOption.No, type = FieldType.String)
    private String measureUnit;
    @ElasticProperty(index = FieldIndexOption.No, type = FieldType.String)
    private String minMeasureUnit;
    @ElasticProperty(index = FieldIndexOption.Analyzed, type = FieldType.String, analyzer = "jd_car_edge_ngram_analyzer")
    private String oeNum;
    @ElasticProperty(index = FieldIndexOption.No, type = FieldType.String)
    private String packageFormat;
    private Long saleNumber;
    private Long sellerId;
    private Long sellerType;
    private Long buyMinNumber;
    private Long buyStepLength;
    private Long packingValue;
    @ElasticProperty(index = FieldIndexOption.No, type = FieldType.String)
    private String packageMeasureUnit;
    @ElasticProperty(type = FieldType.Nested)
    private List<PriceList> priceList;
    @ElasticProperty(type = FieldType.Nested)
    private List<Stock> stockList;
}

class ShopAdapterCar {
    private Long id;
    private Long carBrandId;
    private Long carSeriesId;
    private Long carModelId;
    private Long carPowerId;
    private Long carYearId;
    private Long carId;
    @ElasticProperty(index = FieldIndexOption.Analyzed, type = FieldType.String,subtype = SubType.PinYin)
    private String carName;
}

class Attribute {
    private Long id;
    private Long attrId;
    @ElasticProperty(index = FieldIndexOption.Analyzed, type = FieldType.String)
    private String attrValue;
}

class PriceList {
    private Long id;
    private Long cityId;
    private Double warehousePrice;
    private Double standardPrice;
    private Double redEnvelopePrice;
    private Double yunXiuPrice;
    private Long batchStartAmount;
    private Long batchBuyStepLength;
    private Long batchIsOnSale;
    private Long isOnSale;
}

class Stock {
    private Long id;
    private Long warehouseId;
    private Long goodsNumber;
    private Long qty;

}