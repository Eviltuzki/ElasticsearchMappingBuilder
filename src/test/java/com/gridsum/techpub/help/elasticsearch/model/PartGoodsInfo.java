package com.gridsum.techpub.help.elasticsearch.model;

import com.gridsum.techpub.help.elasticsearch.Name;
import com.gridsum.techpub.help.elasticsearch.annotation.ElasticProperty;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldIndexOption;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldType;
import com.gridsum.techpub.help.elasticsearch.enumtype.SubType;

import java.util.List;
@Name("云修商品")
public class PartGoodsInfo {
    private Long id;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,subtype = SubType.PinYin)
    private String goodsName;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = "jd_car_edge_ngram_analyzer")
    private String goodsFormat;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String oeNum;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String goodsSn;
    private Long minMeasureUnit;
    private Long shopId;
    private Double soldPrice;
    @ElasticProperty(type = FieldType.Nested)
    private List<AdapterCar> adapterCar;
}
class AdapterCar{
    private Long id;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String adapter_models;
}