package com.gridsum.techpub.help.elasticsearch.model;

import com.gridsum.techpub.help.elasticsearch.Name;
import com.gridsum.techpub.help.elasticsearch.annotation.ElasticProperty;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldIndexOption;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldType;
import com.gridsum.techpub.help.elasticsearch.enumtype.SubType;

import java.util.List;
@Name("需求单报价")
public class CloudepcCenterGoods {
    private Long id;
    private Long thirdCateId;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String goodsPic;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = "jd_car_edge_ngram_analyzer")
    private String goodsFormat;
    private Long brandId;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String goodsUnit;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = "jd_car_edge_ngram_analyzer")
    private String oeNumber;
    private Long partId;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,subtype = SubType.PinYin)
    private String partName;
    private Byte isStandardized;
    @ElasticProperty(type = FieldType.Nested)
    private List<Car> car;

}

class Car {
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,subtype = SubType.PinYin)
    private String name;
    private Long id;
    private Long modelId;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,subtype = SubType.PinYin)
    private String brand;

}
