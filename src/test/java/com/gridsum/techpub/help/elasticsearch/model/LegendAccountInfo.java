package com.gridsum.techpub.help.elasticsearch.model;

import com.gridsum.techpub.help.elasticsearch.Name;
import com.gridsum.techpub.help.elasticsearch.annotation.ElasticProperty;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldIndexOption;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldType;

import java.util.List;
@Name("云修账户")
public class LegendAccountInfo {
    private Long id;
    private Long shopId;
    private Long customerId;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String customerName;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String mobile;
    @ElasticProperty(type = FieldType.Nested)
    private List<MemberCars> memberCars;
    @ElasticProperty(type = FieldType.Nested)
    private List<MemberCard> memberCard;
}
class MemberCars{
    private Long id;
    private Boolean isExtends;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String)
    private String license;
}
class MemberCard{
    private Long id;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String)
    private String cardNumber;
    private Long cardTypeId;
    private Double balance;
    private Long gmtCreate;
    private Long gmtModified;
    private Long expireDate;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String typeName;
}
