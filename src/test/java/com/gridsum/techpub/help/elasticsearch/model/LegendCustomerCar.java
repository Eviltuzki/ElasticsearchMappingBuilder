package com.gridsum.techpub.help.elasticsearch.model;

import com.gridsum.techpub.help.elasticsearch.Name;
import com.gridsum.techpub.help.elasticsearch.annotation.ElasticProperty;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldIndexOption;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldType;
import com.gridsum.techpub.help.elasticsearch.enumtype.SubType;

import java.util.Date;
import java.util.List;
@Name("车辆关键字")
public class LegendCustomerCar {
    private Long id;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date gmtCreate;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date gmtModified;
    private Long customerId;
    private Integer shopId;
    private Long carPowerId;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String carPower;
    private Integer carYearId;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String carYear;
    private Integer carSeriesId;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String car_series;
    private Integer carBrandId;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String carBrand;
    private Integer carModelId;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String carModel;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String importInfo;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String latestPrecheck;
    private Integer precheckCount;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String appointCout;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date latestMaintain;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date remindInsuranceTime;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date remindKeepupTime;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = "jd_car_edge_ngram_analyzer")
    private String license;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date keepupTime;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String engineNo;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String vin;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date insuranceTime;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date remindAuditingime;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String carCompany;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String maintainCount;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date auditingTime;
    @ElasticProperty(type = FieldType.Date,format = "yyyy-MM-dd hh:mm:ss")
    private Date buyTime;
    private Double expenseAmount;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String latestRepair;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String latestPaied;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String carGearBox;
    private Long mileage;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String upkeepMileage;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String contact;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,subtype = SubType.PinYin)
    private String customerName;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String,subtype = SubType.PinYin)
    private String company;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = "jd_car_edge_ngram_analyzer")
    private String mobile;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = "jd_car_edge_ngram_analyzer")
    private String source;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,analyzer = "jd_car_edge_ngram_analyzer")
    private String contactMobile;
    private List<String> tags;
}
