package com.gridsum.techpub.help.elasticsearch.model;

import com.gridsum.techpub.help.elasticsearch.Name;
import com.gridsum.techpub.help.elasticsearch.annotation.ElasticProperty;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldIndexOption;
import com.gridsum.techpub.help.elasticsearch.enumtype.FieldType;
import com.gridsum.techpub.help.elasticsearch.enumtype.SubType;
@Name("全车件客户")
public class PartCustomer {
    private Long id;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,subtype = SubType.PinYin)
    private String customerName;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String,subtype = SubType.PinYin)
    private String customerFormalName;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String defaultAccountCommunication;
    @ElasticProperty(index = FieldIndexOption.No,type = FieldType.String)
    private String defaultAccountName;
    private Long orgId;
    private Long saleStatus;
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String customerNamePinyin;
}
