# ElasticsearchMappingBuilder

#### 项目介绍

- ES 2.X 6.X Mapping创建辅助工具，根据注解和字段自动生成，支持功能较少，5.X未测试
- 支持分词、不分词。
- 支持子Field。
- 支持Nested


#### 安装教程

无需安装

#### 使用说明

```

public class Case {
    @ElasticProperty(type = FieldType.Nested)
    private EsCourt[] courtNested;
    /**
     * mongoDb Id
     */
    private Integer[] mongoId;
    /**
     * 基础文书ID
     */
    private String id;

    /**
     * 文书标题
     */
    private String title;
    /**
     * 文书案号（唯一）
     */
    @ElasticProperty(index = FieldIndexOption.NotAnalyzed,type = FieldType.String)
    private String serialNumber;


    private List<EsCourt> court;

    private Date decisionDate;


    private int caseid;
    @ElasticProperty(index = FieldIndexOption.Analyzed,type = FieldType.String)
    private String[] test;


}



String builder = MappingBuilder.builder(Case.class); //ES 2.X

String builder = MappingBuilder6.builder(Case.class); //ES 6.X

```


